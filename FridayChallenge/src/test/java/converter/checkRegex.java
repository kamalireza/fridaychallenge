package converter;

import com.google.gson.Gson;

public class checkRegex {
	public static String convertStringToJsonObject(String addressOfString) {
		addressOfString = addressOfString.replace(",", "");
		String[] streetArray = null;
		String housenumber = null;
		String street = null;

		if (addressOfString.contains("No ")) {
			streetArray = addressOfString.split("No");
			if (!streetArray[0].isEmpty() && !streetArray[1].isEmpty()) {
				street = streetArray[0].replaceAll("^\\s+|\\s+$", "");
				housenumber = "No" + streetArray[1];
			}

		} else {
			streetArray = addressOfString.split("\\d+\\s[A-z]$|\\d+[A-z]+|\\d+");

			street = streetArray[0].isEmpty() ? streetArray[1] : streetArray[0];
			housenumber = addressOfString.replace(street, "");

			// Trim space
			street = street.replaceAll("^\\s+|\\s+$", "");
			housenumber = housenumber.replaceAll("^\\s+|\\s+$", "");
		}
		
		return new Gson().toJson(new Address(street, housenumber));
	}

}
