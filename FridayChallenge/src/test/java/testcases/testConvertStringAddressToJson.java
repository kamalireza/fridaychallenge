package testcases;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import converter.checkRegex;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class testConvertStringAddressToJson {
	private String address;
	private String expectionResault;
	private checkRegex Checkregex;

	public testConvertStringAddressToJson(String address, String expectionResault) {
		this.address = address;
		this.expectionResault = expectionResault;
	}

	@Before
	public void initialize() {
		Checkregex = new checkRegex();
	}

	@Parameterized.Parameters
	public static Collection<Object[]> input() {
		return Arrays.asList(new Object[][] { { "Winterallee 3", "{\"street\":\"Winterallee\",\"housenumber\":\"3\"}" },
				{ "Musterstrasse 45", "{\"street\":\"Musterstrasse\",\"housenumber\":\"45\"}" },
				{ "Blaufeldweg 123B", "{\"street\":\"Blaufeldweg\",\"housenumber\":\"123B\"}" },
				{ "Am B�chle 23", "{\"street\":\"Am B�chle\",\"housenumber\":\"23\"}" },
				{ "Auf der Vogelwiese 23 b", "{\"street\":\"Auf der Vogelwiese\",\"housenumber\":\"23 b\"}" },
				{ "4, rue de la revolution", "{\"street\":\"rue de la revolution\",\"housenumber\":\"4\"}" },
				{ "200 Broadway Av", "{\"street\":\"Broadway Av\",\"housenumber\":\"200\"}" },
				{ "Calle Aduana, 29", "{\"street\":\"Calle Aduana\",\"housenumber\":\"29\"}" },
				{ "Calle 39 No 1540", "{\"street\":\"Calle 39\",\"housenumber\":\"No 1540\"}" }

		});

	}

	@Test
	public void test_AddressOfStringToJson() {
		System.out.println("Test case run with parameters : " + expectionResault);
		assertEquals(expectionResault, checkRegex.convertStringToJsonObject(address));
	}

}
