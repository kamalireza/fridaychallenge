package runtestcases;

import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

import testcases.testConvertStringAddressToJson;

public class test {

	public static void main(String[] args) {

		Result result = JUnitCore.runClasses(testConvertStringAddressToJson.class);
		for (Failure failure : result.getFailures()) {
			System.out.println(failure.toString());
		}
		if (result.wasSuccessful())
			System.out.println("***Test case successfully run***");
		else
			System.out.println("---Test case fail to run---");
	}
}
